version: "3.8"

networks:
  weskit:
    driver: overlay
    driver_opts:
      encrypted: "false"

secrets:
  weskit.key:
    file: ../certs/weskit.key
  weskit.crt:
    file: ../certs/weskit.crt
  weskit.pem:
    file: ../certs/weskit.pem

volumes:
  redis-data:

services:

  rest:
    image: registry.gitlab.com/one-touch-pipeline/weskit/api:dev-2.0.0

    depends_on:
      - database
      - result_broker
    environment:
      WESKIT_CONFIG: /home/weskit/weskit_config/config.yaml
      WESKIT_LOG_CONFIG: /home/weskit/weskit_config/log-config.yaml
      WESKIT_VALIDATION_CONFIG:
      WESKIT_UWSGI_INI: /weskit/uwsgi_server/uwsgi.ini
      WESKIT_DATABASE_URL: mongodb://database:27017
      WESKIT_DATA: /data
      RESULT_BACKEND: redis://result_broker:6379
      BROKER_URL: redis://result_broker:6379
      FLASK_APP: weskit
      WESKIT_WORKFLOWS: /weskit/workflows
    volumes:
      - ../tests/workflows/:/weskit/workflows:ro
      - ../weskit_config/:/home/weskit/weskit_config/:ro
      - ../tests/data/:/data/:rw
    networks:
      - weskit
    working_dir: /weskit
    entrypoint: ["bash", "-i", "-c"]
    command: ["conda activate weskit && uwsgi --ini /weskit/uwsgi_server/uwsgi.ini"]
    deploy:
        mode: replicated
        replicas: 1
        labels:
            - "traefik.enable=true"
            - "traefik.http.routers.weskit_rest.rule=PathPrefix(`/ga4gh/wes`)"
            - "traefik.http.routers.weskit_rest.tls=true"
            - "traefik.http.services.weskit_rest.loadbalancer.server.port=5000"

  database:
    image: mongo:5.0.6
    networks:
      - weskit
    deploy:
      # Deploy on the manager only, because the data volume is local on that machine
      placement:
        constraints: [node.role == manager]

  result_broker:
    image: redis:6.2.6-alpine
    volumes:
      - ../weskit_config/redis.conf:/usr/local/etc/redis/redis.conf:ro
      # Used for persistence of the broker data.
      # https://hub.docker.com/_/redis
      - redis-data:/data
    networks:
      - weskit
    deploy:
      # Deploy on the manager only, because the data volume is local on that machine
      placement:
        constraints: [node.role == manager]
    command: [ "redis-server", "/usr/local/etc/redis/redis.conf" ]

  worker:
    image: registry.gitlab.com/one-touch-pipeline/weskit/api:dev-2.0.0
    depends_on:
      - result_broker
    environment:
      WESKIT_CONFIG: /home/weskit/weskit_config/config.yaml
      RESULT_BACKEND: redis://result_broker:6379
      BROKER_URL: redis://result_broker:6379
      WESKIT_WORKFLOWS: /weskit/workflows
    working_dir: /weskit
    entrypoint: ["bash", "-i", "-c"]
    command: ["conda activate weskit && celery --app weskit.tasks.celery_worker.celery_app worker --loglevel DEBUG --concurrency 1"]
    volumes:
      - ../tests/workflows/:/weskit/workflows:ro
      - ../weskit_config/:/home/weskit/weskit_config/:ro
      - ../tests/data/:/data/:rw
    networks:
      - weskit
    deploy:
      mode: replicated
      replicas: 1

  dashboard:
    image: "registry.gitlab.com/one-touch-pipeline/weskit/dashboard:latest"
    environment:
      WESKIT_DASHBOARD_URL: "https://localhost:443"
      WESKIT_API_URL: "http://rest:5000"
      WESKIT_LOGIN: "false"
      WESKIT_INFO__info_text: "This is a demo website"
      WESKIT_INFO__imprint_name: "My Name"
      WESKIT_INFO__imprint_address: "Somestreet 13, Somecity 12345"
      WESKIT_INFO__imprint_email: "bla@blub.com"
    working_dir: /dashboard
    entrypoint: ["bash", "-i", "-c"]
    command: [ "conda activate dashboard && uwsgi --ini /dashboard/uwsgi_server/uwsgi.ini" ]
    networks:
      - weskit
    deploy:
      mode: replicated
      replicas: 1
      labels:
        - "traefik.enable=true"
        - "traefik.http.routers.weskit_dashboard.rule=PathPrefix(`/`)"
        - "traefik.http.routers.weskit_dashboard.tls=true"
        - "traefik.http.services.weskit_dashboard.loadbalancer.server.port=5001"

  traefik:
    image: traefik:v2.7
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - ../traefik/:/configuration/
    command:
      - --providers.file.directory=/configuration/
      - --providers.file.watch=true
      - --api.dashboard=true # see https://docs.traefik.io/v2.0/operations/dashboard/#secure-mode for how to secure the dashboard
      - --providers.docker=true
      - --providers.docker.swarmMode=true
      - --providers.docker.exposedbydefault=false
      - --providers.docker.network=weskit_weskit
      - --entrypoints.web.address=:80
      - --entrypoints.websecure.address=:443
      - --entrypoints.web.http.redirections.entrypoint.to=websecure
      - --log.level=DEBUG
    ports:
      - 80:80
      - 443:443
    networks:
      - weskit
    deploy:
      labels:
        - traefik.enable=true
    secrets:
      - source: weskit.key
        target: /certs/traefik.key
      - source: weskit.crt
        target: /certs/traefik.crt
      - source: weskit.pem
        target: /certs/traefik.pem